# nlp
1. S-BERT GitHub: https://github.com/UKPLab/sentence-transformers
2. Knowledge distillation script:  https://github.com/UKPLab/sentence-transformers/tree/master/examples/training/distillation/model_distillation.py
3. Knowledge distillation research paper: https://arxiv.org/abs/2004.09813
4. Open parallel corpus database: https://opus.nlpl.eu/
5. Dialog corpuses database: https://kili-technology.com/blog/chatb

